############################## gtsummary基线图 #############################
##### 科研工具9 基线图
library(haven)
library(plyr)
library(dplyr)
library(arsenal)
library(readr)
library(gtsummary)

setwd('父级路径')

paper.data<- read_csv("fromdata.csv")



paper.dataUsecol <- paper.data[,c('RIDAGEYR','Sex','Raceethnicity','Educationlevel','BMIgroup','rpa','smoke_group','diabetesindex',
                                  'LBXVIC','LBXSATSI','LBDHDD','LUXCAPM','fibrosis_group')]


tblsum<- tbl_summary(paper.dataUsecol,
               missing = 'no', 
               # 通过属性分组
               by = fibrosis_group,
               # 分区yes 需要YES 和NO 如果采用Yes 和No 不知道是不是有变量冲突导致分类不显示 只显示全部的
               # 进行设置显示维度
               include = c(RIDAGEYR,Sex,Raceethnicity,Educationlevel,BMIgroup,rpa,smoke_group,diabetesindex,
                           LBXVIC,LBXSATSI,LBDHDD,LUXCAPM),
               # type = all_continuous() ~ "continuous2",
               # 批量给变量起别名
               label = list(RIDAGEYR ~ "Age (years)",
                            LUXCAPM ~ "Median CAP (dB/m)",
                            Sex ~ "Gender, n (%)a",
                            rpa ~ "Recreational physical activity, n (%)a",
                            Raceethnicity ~ "Race/ethnicity, n (%)a",
                            Educationlevel ~ "Education level, n (%)a",
                            BMIgroup ~ "BMI group, n (%)a",
                            smoke_group ~ "Smoking status, n (%)a",
                            LBXVIC ~ "Serum vitamin C (mg/dL)",
                            diabetesindex ~ "Diabetes, n (%)a",
                            LBXSATSI ~ "ALT (IU/L)",
                            LBDHDD ~ "HDL-cholesterol (mg/dL)"),
               statistic = list(
                 # 分别对应数值型和分类变量 后是需要显示表达式
                 # all_continuous() ~ "{mean}±{sd}",
                 all_continuous2() ~ c(
                   "{N_nonmiss}",
                   "{median} ({p25}, {p75})",
                   "{min}, {max}"
                 ),
                 all_categorical() ~ "(N= {n} percent {p}%)"
               ),
               digits = list(all_continuous() ~ 2
                             # ,all_categorical() ~ 3
               )
)%>%
  add_n(
    col_label = "**N**"
  )%>%
  add_p(pvalue_fun = ~ style_pvalue(.x, digits = 2)) %>%
  add_overall()%>%
  bold_labels() %>%   #label 粗体
  # modify_header(stat_1 ~ "**No** (n={n})") %>%
  # modify_header(stat_2 ~ "**Yes** (n={n})") %>%
  modify_spanning_header(c("stat_1", "stat_2") ~ "**Treatment Received**") %>%
  modify_caption("**Table 1. Test Three wire table**")


tblsum%>%as_flex_table()%>%flextable::save_as_image(path = 'aaa.tiff')
