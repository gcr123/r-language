library(randomForestSRC)
library(randomForest)
data("veteran")
head(veteran)

# veteran$trt <- veteran$status
##   trt celltype time status karno diagtime age prior
## 1   1        1   72      1    60        7  69     0
## 2   1        1  411      1    70        5  64    10
## 3   1        1  228      1    60        3  38     0
## 4   1        1  126      1    60        9  63    10
## 5   1        1  118      1    70       11  65    10
## 6   1        1   10      1    20        5  49     0
# 这个东西每次结果不一致,导致复现每次都不一样,,,,
set.seed(1200)
rfsrc_fit <- rfsrc(
  Surv(time,status)~., #公式
  ntree = 2000,         # 树的棵树 
  nsplit = 5,          # 最小节点数
  importance = TRUE,   #变量重要性
  tree.err=TRUE,       # 误差
  data=veteran         # 数据集
)

plot(rfsrc_fit)

plot(get.tree(rfsrc_fit,3))

matplot(rfsrc_fit$time.interest,
        100*t(rfsrc_fit$survival.oob[1:5,]),
        xlab = "time",
        ylab = "Survival",
        type="l",lty=1,
        lwd=2)

plot.survival(rfsrc_fit,subset=1:5)

vipm_obj <- subsample(rfsrc_fit)
plot(vipm_obj)



