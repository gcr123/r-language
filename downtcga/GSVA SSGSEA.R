# https://mp.weixin.qq.com/s/mRwgS6upMExvWyeMNNl7ig
#前面，我们介绍过了差异基因的功能富集分析，今天，我们对这部分的内容作一些补充
# 主要介绍一下 GEVA、ssGSEA 和单基因的富集分析
#  Gene Set Variation Analysis（GSVA）与 GSEA 的原理类似

# 先获取基因表达矩阵，我们使用 TCGA 肺腺癌和肺鳞癌各 10 个样本的 read counts 数据

# 这个暂时搞不了 需要让讲师看下 我这边出错 看的是有问题
library(TCGAbiolinks)
get_count <- function(cancer, n = 10) {
  query <- GDCquery(
    project = cancer,
    data.category = "Gene expression",
    data.type = "Gene expression quantification",
    platform = "Illumina HiSeq",
  
    sample.type = c("Primary Tumor")
  )
  query <- GDCquery(
    project = variable,
    data.category = "Simple Nucleotide Variation",
    access = "open",
    data.type = "Masked Somatic Mutation"
  )
  # 选择 n 个样本
  query$results[[1]] <-  query$results[[1]][1:n,]
  GDCdownload(query)
  # 获取 read count
  exp.count <- GDCprepare(
    query,
    summarizedExperiment = TRUE,
  )
  return(exp.count)
}
luad.count <- get_count("TCGA-LUAD")
