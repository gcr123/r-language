library(SummarizedExperiment)
library(TCGAbiolinks)

projects <- TCGAbiolinks::getGDCprojects()$project_id ##获取癌症名字

projects <- projects[grepl('^TCGA', projects, perl=TRUE)]

projects

# 2024年6月7日09:43:15 批量下载保存到服务器上 先下载到本地 后期快速使用

for (variable in projects) {
  print(variable)
  str1 <- variable
  str2 <- "_download.rda"
  result <- paste0(str1, str2)
  
  print(result)
  query <- GDCquery(
    project = variable,
    data.category = "Simple Nucleotide Variation",
    access = "open",
    data.type = "Masked Somatic Mutation"
  )
  GDCdownload(query,method = "api")
  GDCprepare(query = query, save = TRUE, save.filename = result)
}





maf <- GDCprepare(query)
dim(maf)
laml.maf = maf 
save.image(file = "LGG.RData")

query <- GDCquery(project = "TCGA-LGG",
                  data.category = "Copy Number Variation",
                  data.type = "Masked Copy Number Segment")
GDCdownload(query,method = "api")
LGG_CNV_download <- GDCprepare(query = query, save = TRUE, save.filename = "LGG_CNV_download.rda")
