#### 堆积图 ####
try({
  setwd("F:/rproject/r-language/lilongyuan2/堆积柱状图")
})

try({
  setwd('/home/r_temp//2024726/1629278/12/1721960000550/')
})


library(ggplot2)
library(stringr)
library(reshape2)

readFile <- function(x)  {
  data<- ""
  if (file.exists(x)) {
    if (grepl(".csv", x, ignore.case = TRUE) ) {
      print("读取csv文件")
      data <- read.csv(x)

    }else{
      print("读取txt文件")
      data <- read.table(x, header=TRUE, sep="\t")
    }
  }
  return(data)
}



#读取数据
#做成动态获取 不在是写死了

mydata<- ""
excelPath <- '/home/r_temp//2024726/1629278/12/1721960000550/'
mydata<- readFile(excelPath)

colnameList = colnames(mydata)
# 用户需要输入分析的维度
varMeltCol = 'Phylum'
# 排除上面的字符剩下的
colnameListAna<-colnameList[!colnameList%in%c(varMeltCol)]
#数据转置
mydata<-melt(mydata,id.vars=varMeltCol)
#设置新罗马字体 在文字显示的时候可以更加美观
windowsFonts(A=windowsFont("Times New Roman"),
             B=windowsFont("Arial"))

#定义X轴的顺序 暂时用不到

#绘图
p1 <- ggplot(mydata,aes(variable, value),position="stack") +
  scale_x_discrete(limits=colnameListAna)+
  geom_bar(aes(fill = Phylum), stat = "identity",color="black",size=0.4,
           position = "fill", width = 0.6,data=mydata)+
  scale_fill_manual(values=c("#56B4E9",'gray', '#CCEBC5', '#BC80BD', '#FCCDE5',
                             '#B3DE69', '#FDB462', '#80B1D3', '#FB8072',
                             '#BEBADA', '#FFFFB3', '#8DD3C7'))+
  theme(
    axis.title=element_text(size=15,face="plain",color="black"),
    axis.text = element_text(size=15,face="plain",color="black"),
    legend.title=element_text(size=15,face="plain",color="black"),
    legend.position = "right",
    panel.background = element_blank(),
    axis.line = element_line(colour = "black", size = 0.4))+theme_bw()+
  theme(text=element_text(family="A",size=20))+
  theme(axis.ticks.length=unit(-0.25, "cm"),
        axis.text.x = element_text(margin=unit(c(0.5,0.5,0.5,0.5), "cm")),
        axis.text.y = element_text(margin=unit(c(0.5,0.5,0.5,0.5), "cm")) )
 # p1
p2<- ggplot(mydata,aes(variable, value),position="stack") +
  scale_x_discrete(limits=colnameListAna)+
   geom_bar(aes(fill = Phylum), stat = "identity",color="black",size=0.4,
            position = "stack", width = 0.6,data=mydata)+

  scale_fill_manual(values=c("#56B4E9",'gray', '#CCEBC5', '#BC80BD', '#FCCDE5',
                             '#B3DE69', '#FDB462', '#80B1D3', '#FB8072',
                             '#BEBADA', '#FFFFB3', '#8DD3C7'))+
  theme(
    axis.title=element_text(size=15,face="plain",color="black"),
    axis.text = element_text(size=15,face="plain",color="black"),
    legend.title=element_text(size=15,face="plain",color="black"),
    legend.position = "right",
    panel.background = element_blank(),
    axis.line = element_line(colour = "black", size = 0.4))+theme_bw()+
  theme(text=element_text(family="A",size=20))+
  theme(axis.ticks.length=unit(-0.25, "cm"),
        axis.text.x = element_text(margin=unit(c(0.5,0.5,0.5,0.5), "cm")),
        axis.text.y = element_text(margin=unit(c(0.5,0.5,0.5,0.5), "cm")) )
# p2
# library(patchwork)


pall <- p1+p2
png(file="p1.png")
pall
dev.off()
