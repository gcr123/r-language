#### 堆积图 ####
try({
  setwd("F:/rproject/r-language/lilongyuan2/堆积柱状图")
})

try({
  setwd('父级路径')
})


library(ggplot2)
library(stringr)
library(reshape2)

readFile <- function(x)  {
  data<- ""
  if (file.exists(x)) {
    if (grepl(".csv", x, ignore.case = TRUE) ) {
      print("读取csv文件")
      data <- read.csv(x)
      
    }else{
      print("读取txt文件")
      data <- read.table(x, header=TRUE, sep="\t")
    }
  }
  return(data)
}
#读取数据
#做成动态获取 不在是写死了
mydata<- ""
excelPath <- 'Untitled.txt'
mydata<- readFile(excelPath)

# 生成一个随机矩阵
# set.seed(123)
# randowmMatrix<-matrix(runif(9, min = 0, max = 10), nrow = 10, ncol = 10)
# # 列名开头
# colnameParam <- 'C'
# # 行名开头
# rownameParam <- 'R'
# colSize<-ncol(randowmMatrix)
# rowSize<-nrow(randowmMatrix)
# rownames(randowmMatrix) <- paste0(rownameParam, 1:rowSize)
# colnames(randowmMatrix) <- paste0(colnameParam, 1:colSize)
# uppercase_letters <- LETTERS[1:10]
# randowmMatrix<-as.data.frame(randowmMatrix)
# randowmMatrix$Phylum<- uppercase_letters
# mydata<- randowmMatrix
# 
#  write.table(mydata,file = 'Untitled.txt',sep="\t",quote=F,col.names=T,row.names = T )

colnames(mydata)
rownames(mydata)<- c(1:10)
colnameList = colnames(mydata)
# 用户需要输入分析的维度
varMeltCol = 'Phylum'
# varMeltCol = 'C1'
# 排除上面的字符剩下的
colnameListAna<-colnameList[!colnameList%in%c(varMeltCol)]
#数据转置
mydata<-melt(mydata,id.vars=varMeltCol)
#设置新罗马字体 在文字显示的时候可以更加美观
# windowsFonts(A=windowsFont("Times New Roman"),
#              B=windowsFont("Arial"))

#定义X轴的顺序 暂时用不到

#绘图
p1 <- ggplot(mydata,aes(variable, value),position="stack") +
  scale_x_discrete(limits=colnameListAna)+
  geom_bar(aes(fill = Phylum), stat = "identity",color="black",size=0.4,
           position = "fill", width = 0.6,data=mydata)+
  scale_fill_manual(values=c("#56B4E9",'gray', '#CCEBC5', '#BC80BD', '#FCCDE5',
                             '#B3DE69', '#FDB462', '#80B1D3', '#FB8072',
                             '#BEBADA', '#FFFFB3', '#8DD3C7'))+
  theme(
    axis.title=element_text(size=15,face="plain",color="black"),
    axis.text = element_text(size=15,face="plain",color="black"),
    legend.title=element_text(size=15,face="plain",color="black"),
    legend.position = "right",
    panel.background = element_blank(),
    axis.line = element_line(colour = "black", size = 0.4))+theme_bw()+
  theme(text=element_text(family="A",size=20))+
  theme(axis.ticks.length=unit(-0.25, "cm"),
        axis.text.x = element_text(margin=unit(c(0.5,0.5,0.5,0.5), "cm")),
        axis.text.y = element_text(margin=unit(c(0.5,0.5,0.5,0.5), "cm")) )
# p1
p2<- ggplot(mydata,aes(variable, value),position="stack") +
  scale_x_discrete(limits=colnameListAna)+
  geom_bar(aes(fill = Phylum), stat = "identity",color="black",size=0.4,
           position = "stack", width = 0.6,data=mydata)+
  
  scale_fill_manual(values=c("#56B4E9",'gray', '#CCEBC5', '#BC80BD', '#FCCDE5',
                             '#B3DE69', '#FDB462', '#80B1D3', '#FB8072',
                             '#BEBADA', '#FFFFB3', '#8DD3C7'))+
  theme(
    axis.title=element_text(size=15,face="plain",color="black"),
    axis.text = element_text(size=15,face="plain",color="black"),
    legend.title=element_text(size=15,face="plain",color="black"),
    legend.position = "right",
    panel.background = element_blank(),
    axis.line = element_line(colour = "black", size = 0.4))+theme_bw()+
  theme(text=element_text(family="A",size=20))+
  theme(axis.ticks.length=unit(-0.25, "cm"),
        axis.text.x = element_text(margin=unit(c(0.5,0.5,0.5,0.5), "cm")),
        axis.text.y = element_text(margin=unit(c(0.5,0.5,0.5,0.5), "cm")) )
# p2
library(patchwork)
pall <- p1+p2
pall
png(file="p1.png", width=980, height=980)
pall
dev.off()
