library("pheatmap")
library("colorRamps")
library("ComplexHeatmap")
#BiocManager::install("ComplexHeatmap")
library("ComplexHeatmap")
##install.packages("vegan")
library("vegan")
library(circlize)
A=read.table("分组.txt",sep="\t",header=T,check.names=F)
B=read.table("risk合并后rt.txt",sep="\t",header=T,check.names=F)
C=read.table("整理后趋化因子表达矩阵.txt",sep="\t",header=T,check.names=F)
C <- decostand(C,"standardize",MARGIN = 1)
C <- na.omit(C)
#data.1 <- as.data.frame(scale(data))#第二种方法，但是这个默认是对列进行处理
View(C1)
#c查看其标准差
apply(C,1,sd) 

rownames(A) <- A[,1]
Brow<-rownames(B)
Ccol<-colnames(C)
common_names<-intersect(Brow,Ccol)
comgene <- intersect(rownames(B),colnames(C))
C2<-C[,Ccol%in%common_names]
A <- A[comgene,]


col_fun = colorRamp2(c(-1, 0, 1), c("navy","white","firebrick3"), space = "RGB")
##注释条
ha <- HeatmapAnnotation(
  bar = sample(letters[1:3], 10, replace = TRUE),
  col = list(bar = c("a" = "red", "b" = "green", "c" = "blue"))
)
draw(ha)
#多个注释条
clinical <- B
col_risk <- c("high" = "#666111", "low" = "#A65628")
col_MGMT <- c("Methylated"="#4DAF4A","Unmethylated"="#E41A1C")
col_riskscore <- colorRamp2(c(0, 50, 100), c("navy","white","firebrick3"), space = "RGB")
columnanno <- HeatmapAnnotation(risk=A$risk,
                                MGMT=clinical$MGMT,
                                risksocre = clinical$TCGA.RS,
                                col = list(
                                  risk = col_risk,
                                  MGMT = col_MGMT,
                                  risksocre = col_riskscore
                                ),
                                ##simple_anno_size = unit(1, "cm")##注释条高度
                                show_annotation_name = T
)
draw(columnanno)
##split <- rep(1:5, each = 10)##分割
p1 <- Heatmap(C2,
        col = col_fun,
        cluster_rows = F,##关闭行聚类
        cluster_columns = F,##关闭列聚类##show_row_dend = T, show_column_dend = F聚类但是不显示聚类树
        #show_row_names = F,##行名
        show_column_names = F, ##列名
        #column_names_rot = 45，##列名旋转方向
        #column_names_side = "top",#上方列名
        #column_dend_side = "bottom"，#同上
        ##row_km = 2, #k-mean法分割行
        ##column_split = split,##自定义分割
        bottom_annotation = columnanno
)
p1
