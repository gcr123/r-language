# 加载e1071包
#if (!require(e1071)) install.packages("e1071")
library(e1071)
library(caret)
library(pROC)
library(MatchIt)
library(dplyr)
# 设置工作目录并加载数据
setwd("${path}")



mydata <- read.csv("bibliometric_nuomotu.csv")

cols<-colnames(mydata)[3:38]
colnames(mydata)
cols
colsdou<- paste0("'",cols,"'")
independent_and<- paste0(cols,collapse = "+")
independent_dou<- paste0(colsdou,collapse = ",")
# 确保 group_best 是因子，并且值为 "event_0" 和 "event_1"
mydata$group_best <- factor(mydata$group_best, levels = c("0", "1"), labels = c("event_0", "event_1"))


# 使用 matchit 进行 1:3 匹配
m.out <- matchit(as.formula(paste0("group_best~",independent_and)), 
                 data = mydata, 
                 method = "nearest", 
                 ratio = 3)

# 提取匹配后的数据
matched_data <- match.data(m.out)
# 训练SVM模型
model_svm <- svm(
  as.formula(paste0("group_best~",independent_and)), 
  data = matched_data,
  kernel = "radial",
  cost = 1,
  gamma = 0.1,
  probability = TRUE
)

# 预测和评估
predictions_svm <- predict(model_svm, newdata = mydata, probability = TRUE)
predicted_probs_svm <- attr(predictions_svm, "probabilities")[,2]
predicted_classes_svm <- predictions_svm

# 添加预测结果到数据集
mydata_with_predictions_svm <- mydata %>%
  mutate(
    group_best_probability = predicted_probs_svm,
    group_best_predicted_class = predicted_classes_svm,
    group_best_score = NA  # SVM没有评分卡
  )
# 计算 group_best 和 group_best_predicted_class 相同的数据数量
matching_rows <- mydata_with_predictions_svm %>%
  filter(group_best == group_best_predicted_class) %>%
  nrow()
# 打印结果
cat("Number of rows where group_best and group_best_predicted_class match:", matching_rows, "\n")
# 可选：计算准确率（匹配行数 / 总行数）
total_rows <- nrow(matched_data)
accuracy <- matching_rows / total_rows
# 打印结果
cat("Number of rows where group_best and group_best_predicted_class match:", accuracy, "\n")
# 这是建模集的结果
write.csv(mydata_with_predictions_svm, file = "mydata_with_predictions.csv", row.names = FALSE)

try({
#### 测试画图 

#### 支持向量图 ####
# 提取支持向量的索引
support_vector_indices <- model_svm$index

# 使用这些索引从原始数据中获取支持向量
support_vectors <- matched_data[support_vector_indices, ]

# 绘制散点图并突出显示支持向量
plot_svm <- ggplot(matched_data, aes(x = auth_sum, y = impact_factor, color = factor(group_best))) +
  geom_point(alpha = 0.6) + # 原始数据点
  geom_point(data = support_vectors, aes(x = auth_sum, y = impact_factor), shape = 4, size = 3, color = "red") + # 支持向量
  labs(title = "Support Vectors Plot", x = "Author Sum", y = "Impact Factor") +
  theme_minimal()

png(filename = 'svm_svm.png',width = 800,height = 800)
# Cairo::CairoTIFF(file="svm_svm.tiff", width=800, height=800,dpi=150)
print(plot_svm)
dev.off()
#### 混淆矩阵 ####
library(caret)
conf_matrix <- confusionMatrix(factor(predicted_classes_svm), factor(matched_data$group_best))
sink("confusionMatrix_svm.txt")
print(conf_matrix)
sink()

#### ROC曲线 ####

# 绘制ROC曲线
try({
  roc_curve <- roc(matched_data$group_sum, predicted_probs_svm)
  png(file="roc_svm.png", width=800, height=800)
  
  plot(roc_curve, col = "#e41a1c", lwd = 2, main = "ROC Curve")
  legend("bottomright", legend = paste("AUC =", round(auc(roc_curve), 2)), bty = "n")
  dev.off()
})


#### 学习曲线 ####
try({
  learning_curve_data <- createDataPartition(mydata$group_best, p = .8, list = FALSE)
  train_indices <- learning_curve_data
  test_indices <- setdiff(1:nrow(mydata), train_indices)
  
  # 训练并评估模型在不同大小的数据集上的表现
  sizes <- seq(0.1, 1, by = 0.1)
  accuracy_list <- sapply(sizes, function(size) {
    subset_indices <- sample(train_indices, size * length(train_indices))
    model <- svm(group_best ~ ., data = mydata[subset_indices, ], kernel = "radial", cost = 1, gamma = 0.1, probability = TRUE)
    predictions <- predict(model, newdata = mydata[test_indices, ])
    conf_matrix <- confusionMatrix(predictions, mydata$group_best[test_indices])
    return(conf_matrix$overall['Accuracy'])
  })
  
  # 绘制学习曲线
  learning_curve_plot <- ggplot(data = data.frame(Size = sizes, Accuracy = accuracy_list), aes(x = Size, y = Accuracy)) +
    geom_line(color = "green", size = 1) +
    geom_point() +
    labs(title = "Learning Curve", x = "Training Set Size Proportion", y = "Accuracy") +
    theme_minimal()
  png(file="learningCurve_svm.png", width=800, height=800,dpi=150)
  print(learning_curve_plot)
  dev.off()
})


#### 画图结束

})


mydata1 <- read.csv("bibliometric_nuomotuRes.csv")
# 预测和评估
predictions_svm <- predict(model_svm, newdata = mydata1, probability = TRUE)
predicted_probs_svm <- attr(predictions_svm, "probabilities")[,2]
predicted_classes_svm <- predictions_svm

# 添加预测结果到数据集
result_with_predictions_svm <- mydata1 %>%
  mutate(
    group_best_probability = predicted_probs_svm,
    group_best_predicted_class = predicted_classes_svm,
    group_best_score = NA  # SVM没有评分卡
  )
# 这是结局的结果
write.csv(result_with_predictions_svm, file = "result_with_predictions.csv", row.names = FALSE)

# 完成
