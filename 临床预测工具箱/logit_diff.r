setwd("/home/jar/zxjar/paper_helper/attachment/0/63")
library(readr)
library(arsenal)
library(dplyr)
library(gtsummary)

setwd('F:/Rproject/临床预测工具箱')
mydata <- read_csv("data.csv")
mydatagcr <- read_csv("data.csv")
# dim(mydata)


mydata<-na.omit(mydata)
# dim(mydata)
# str(mydata)


# 转移数据类型 可以使用 mydatagcr$f_education <- as.factor(mydatagcr$f_education)
mydata = mydata %>% dplyr::mutate(across(f_education, as.factor))
str(mydata)
str(mydatagcr)



library(caret)
dev = mydata[mydata$s_group==1,]
vad = mydata[mydata$s_group==0,]
  
library(compareGroups)
# sink("logit_diff.txt")
dfx= dev %>% select(y_post_thrombotic_syndrome,x_sex,x_bmi,x_smoking) %>% dplyr::rename("group"=1)
str(dfx)
# dfx$x_smoking <- as.factor(dfx$x_smoking)
table<-descrTable(group ~ .,data = dfx,show.ratio = TRUE,show.all = TRUE)
tab3 <- tableby(group~x_sex+x_bmi+x_smoking,dfx,numeric.stats=c("median","q1q3","sd"))
summary(tab3,text = T)
# 这快有错误 需要核实
median(dfx$x_smoking)
# x smoking 默认给
tbl_summary(dfx,by = group,
            statistic = list(all_continuous() ~ "{mean} ({sd})"),
            digits = list(all_continuous() ~ 2,all_categorical() ~ 2),
            )%>%add_overall()%>%add_p(pvalue_fun = function(x) style_pvalue(x, digits = 3))
tbl_summary(dfx,by = group)%>%add_overall()%>%add_p()

table
# sink()

# export2xls(table,file = "logit_diff.xls")

